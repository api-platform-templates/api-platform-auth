# Setup

## Requirements

### Docker
Docker & docker-compose must be installed
* `docker -v` should return the version
* `docker-compose -v` should return the version

### Traefik
If you want to use traefik as reverse proxy
* `docker network create reverse-proxy`
* `make traefik`

## Build dev environment
* `make setup`

## Start dev
* `make start`
