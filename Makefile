.SILENT:
.PHONY: setup help

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":") - 1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

###############
# Environment #
###############

include docker/.env
include .env
export $(shell sed 's/=.*//' .env)
current_dir := $(notdir $(shell pwd))

## Install
install:
	docker-compose run --rm composer install

traefik:
	docker-compose -f docker/traefik/docker-compose.yml up -d

## Setup environment & Install & Build application
setup: build install traefik assets database-create database-migrate start

## Start dev environment
start:
	docker-compose up -d api adminer

## Build
build:
	docker-compose pull
	docker-compose build --pull --parallel

## Create database
database-create:
	docker-compose run --rm console doctrine:database:create

## Run doctrine migrations
database-migrate:
	docker-compose run --rm console doctrine:migrations:migrate --no-interaction

## Create api GUI
assets:
	docker-compose run --rm console assets:install

## Create JWT keys
key: config/jwt/private.pem config/jwt/public.pem

config/jwt/private.pem:
	mkdir -p config/jwt
	openssl genpkey -out config/jwt/private.pem -aes256 -pass pass:"$(JWT_PASSPHRASE)" -algorithm rsa -pkeyopt rsa_keygen_bits:4096

config/jwt/public.pem: config/jwt/private.pem
	openssl pkey -in config/jwt/private.pem -passin pass:"$(JWT_PASSPHRASE)" -out config/jwt/public.pem -pubout
