<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;

class MeController extends AbstractController {

    public function __construct(private Security $security, private UserRepository $userRepository) {}

    public function __invoke(): ?User {

        $user = $this->security->getUser();

        if ($user === null) {
            return null;
        }

        return $this->userRepository->find($user->getUserIdentifier());
    }
}
