<?php declare(strict_types=1);

namespace App\OpenApi\Token;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

class RefreshFactory implements OpenApiFactoryInterface
{

    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        $pathItem = new PathItem(
            post: new Operation(
                operationId: 'postApiTokenRefresh',
                tags: ['Auth'],
                responses: [
                    '200' => [
                        'description' => 'Refresh token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                    'properties' =>
                                        [
                                            'refresh_token' => ['type' => 'string'],
                                        ],
                                ]
                            ]
                        ]
                    ]
                ],
                requestBody: new RequestBody(
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' =>
                                    [
                                        'refresh_token' => ['type' => 'string'],
                                    ],
                            ],
                            'example' => [
                                'refresh_token' => 'eaa19df4a36703bf0f4153bd86910fec4516275ef790c5965fd37497e6f50dec179311017d3d830080c3b5f5d87d5d3b602e5da36d75d24cb16994ed87ffc3f1',
                            ],
                        ]
                    ])
                )
            )
        );
        $openApi->getPaths()->addPath('/api/token/refresh', $pathItem);

        return $openApi;
    }
}
